package vpn;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;


public class VPNGUI extends JFrame {
	public static final String splitter = "||";
	private JPanel contentPane;
	private JTextField textFieldPort;
	private JTextField textFieldIP;
	private JRadioButton rdbtnClient;
	private JRadioButton rdbtnServer;
	private JRadioButton rdbtnDebug;
	private JButton btnConnect;
	private JButton btnSendData;
	private JButton btnContinue;
	private JLabel lblPort;
	private JLabel lblMode;
	private JLabel lblIP;
	private JLabel lblDataReceived;
	private JLabel lblDataEncryptReceived;
	private JLabel lblDataSent;
	private JLabel lblServerIP;
	private String mode = "";
	private JTextField textFieldSent;
	private JTextPane pnDataReceived;
	private JTextPane pnDataEncryptReceived;
	private JButton startStopBtn;
	private Server server;
	private Client client;
	private JLabel lblSharedSecret;
	private JTextField textFieldSharedSecret;
	private JLabel lblDebugLog;
	private JTextPane textPaneDebugLog;
	
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VPNGUI frame = new VPNGUI();
					frame.setTitle("VPN");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private class clientServerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			String command = e.getActionCommand();
			if(command.equals("Client")){
				if(rdbtnClient.isSelected()){
					try {
						if(server != null)
							server.stop();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					rdbtnServer.setSelected(false);
					//hide server gui
					showServerGUI(false);
					//show client gui
					showClientGUI(true);
				}else{
					rdbtnClient.setSelected(true);
				}
			}else if(command.equals("Server")){
				if(rdbtnServer.isSelected()){
					rdbtnClient.setSelected(false);
					//hide client gui
					showClientGUI(false);
					//show server gui
					showServerGUI(true);
				}else{
					rdbtnServer.setSelected(true);
				}
			}
		}
	}
	
	private class startStopListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(startStopBtn.getText() == "Start"){
				startStopBtn.setText("Stop");
				try {
					server = new Server(Integer.parseInt(textFieldPort.getText()), textFieldSharedSecret.getText(), 5, 23, VPNGUI.this);
					server.start();
				} catch (NumberFormatException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			else {
				startStopBtn.setText("Start");
				enableMessageSending(false);
				pnDataEncryptReceived.setText("");
				pnDataReceived.setText("");
				textPaneDebugLog.setText("");
				try {
					server.stop();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}
	
	private class DebugListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			String command = e.getActionCommand();
			if(command.equals("Debug")){
				if(rdbtnDebug.isSelected()){
					btnContinue.setVisible(true);
					textPaneDebugLog.setVisible(true);
					lblDebugLog.setVisible(true);
					Debug.debug = true;
				}
				else {
					btnContinue.setVisible(false);
					textPaneDebugLog.setVisible(false);
					lblDebugLog.setVisible(false);
					Debug.debug = false;
				}
			}
			else if(command.equals("Continue")){
				Debug.step = true;
			}
		}
	}
	
	private class sendMessageListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			String command = e.getActionCommand();
			if(rdbtnClient.isSelected()){
				if(command.equals("Send")){
					try {
						if (client == null) {
							client = new Client(textFieldIP.getText(), 
											Integer.parseInt(textFieldPort.getText()), textFieldSharedSecret.getText(),
											5, 23, VPNGUI.this);
						}
						client.setMessage(textFieldSent.getText());
						client.start();
					} catch (NumberFormatException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (UnknownHostException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
			else {
				server.sendMessage(textFieldSent.getText());
			}
		}
	}
	
	private void showServerGUI(boolean set){
		textFieldSharedSecret.setVisible(set);
		rdbtnDebug.setVisible(set);
		lblSharedSecret.setVisible(set);
		lblIP.setVisible(set);
		lblServerIP.setVisible(set);
		rdbtnServer.setVisible(set);
		lblPort.setVisible(set);
		textFieldPort.setVisible(set);
		lblDataReceived.setVisible(set);
		pnDataReceived.setVisible(set);
		lblDataEncryptReceived.setVisible(set);
		pnDataEncryptReceived.setVisible(set);
		startStopBtn.setVisible(set);
		contentPane.repaint();
	}
	
	private void showClientGUI(boolean set){
		textFieldSharedSecret.setVisible(set);
		rdbtnDebug.setVisible(set);
		lblSharedSecret.setVisible(set);
		lblServerIP.setVisible(false);
		rdbtnClient.setVisible(set);
		lblPort.setVisible(set);
		textFieldPort.setVisible(set);
		lblIP.setVisible(set);
		textFieldIP.setVisible(set);
		lblDataSent.setVisible(set);
		textFieldSent.setVisible(set);
		btnSendData.setVisible(set);
		lblDataReceived.setVisible(set);
		pnDataReceived.setVisible(set);
		lblDataEncryptReceived.setVisible(set);
		pnDataEncryptReceived.setVisible(set);
		contentPane.repaint();
	}
	
	/**
	 * Create the frame.
	 */
	public VPNGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 650);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//Mode Selection
		
		lblMode = new JLabel("Mode");
		lblMode.setBounds(12, 33, 56, 16);
		contentPane.add(lblMode);
		
		rdbtnClient = new JRadioButton("Client");
		rdbtnClient.setBounds(8, 57, 127, 25);
		rdbtnClient.setActionCommand("Client");
		rdbtnClient.addActionListener(new clientServerListener());
		contentPane.add(rdbtnClient);
		
		rdbtnServer = new JRadioButton("Server");
		rdbtnServer.setBounds(8, 93, 127, 25);
		rdbtnServer.setActionCommand("Server");
		rdbtnServer.addActionListener(new clientServerListener());
		contentPane.add(rdbtnServer);
		
		//Port Selection
		
		lblPort = new JLabel("Port");
		lblPort.setBounds(8, 150, 56, 16);
		contentPane.add(lblPort);
		
		textFieldPort = new JTextField();
		textFieldPort.setBounds(8, 165, 116, 22);
		contentPane.add(textFieldPort);
		textFieldPort.setColumns(10);
		
		//IP Selection
		
		lblIP = new JLabel("IP Address");
		lblIP.setBounds(8, 195, 116, 16);
		contentPane.add(lblIP);
		
		textFieldIP = new JTextField();
		textFieldIP.setBounds(8, 210, 116, 22);
		contentPane.add(textFieldIP);
		textFieldIP.setColumns(10);
		
		// Start Stop Button
		
		startStopBtn = new JButton("Start");
		startStopBtn.setBounds(8, 292, 97, 25);
		startStopBtn.setActionCommand("StartStop");
		startStopBtn.addActionListener(new startStopListener());
		contentPane.add(startStopBtn);
		
		// Sent data
		
		lblDataSent = new JLabel("Data to be Sent");
		lblDataSent.setBounds(180, 33, 100, 16);
		contentPane.add(lblDataSent);
		
		textFieldSent = new JTextField();
		textFieldSent.setBounds(180, 58, 116, 22);
		contentPane.add(textFieldSent);
		textFieldSent.setColumns(10);
		
		btnSendData = new JButton("Send Data");
		btnSendData.setBounds(308, 57, 97, 25);
		btnSendData.setActionCommand("Send");
		btnSendData.addActionListener(new sendMessageListener());
		contentPane.add(btnSendData);
		
		// Received data
		
		lblDataReceived = new JLabel("Plain Text");
		lblDataReceived.setBounds(180, 150, 169, 16);
		contentPane.add(lblDataReceived);
		
		pnDataReceived = new JTextPane();
		pnDataReceived.setBounds(180, 175, 575, 75);
		pnDataReceived.setEditable(false);
		contentPane.add(pnDataReceived);
		
		// Received Encrypted data
		
		lblDataEncryptReceived = new JLabel("Data as received");
		lblDataEncryptReceived.setBounds(180, 258, 169, 16);
		contentPane.add(lblDataEncryptReceived);
		
		pnDataEncryptReceived = new JTextPane();
		pnDataEncryptReceived.setBounds(180, 280, 575, 75);
		pnDataEncryptReceived.setEditable(false);
		contentPane.add(pnDataEncryptReceived);
		
		rdbtnDebug = new JRadioButton("Debug");
		rdbtnDebug.setBounds(8, 320, 127, 25);
		rdbtnDebug.setActionCommand("Debug");
		rdbtnDebug.addActionListener(new DebugListener());
		contentPane.add(rdbtnDebug);
		
		btnContinue = new JButton("Continue");
		btnContinue.setBounds(8, 350, 97, 25);
		btnContinue.setActionCommand("Continue");
		btnContinue.addActionListener(new DebugListener());
		contentPane.add(btnContinue);
		
		lblServerIP = new JLabel(getIP());
		lblServerIP.setBounds(8, 210, 116, 16);
		contentPane.add(lblServerIP);
		
		lblSharedSecret = new JLabel("Shared Secret");
		lblSharedSecret.setBounds(8, 240, 112, 16);
		contentPane.add(lblSharedSecret);
		
		textFieldSharedSecret = new JTextField();
		textFieldSharedSecret.setBounds(8, 255, 116, 22);
		contentPane.add(textFieldSharedSecret);
		textFieldSharedSecret.setColumns(10);
		
		lblDebugLog = new JLabel("Debug Log");
		lblDebugLog.setBounds(180, 359, 100, 16);
		contentPane.add(lblDebugLog);
		
		textPaneDebugLog = new JTextPane();
		textPaneDebugLog.setBounds(180, 383, 575, 200);
		textPaneDebugLog.setEditable(false);
		contentPane.add(textPaneDebugLog);
		
		showClientGUI(false);
		showServerGUI(false);
		
		btnContinue.setVisible(false);
		lblDebugLog.setVisible(false);
		textPaneDebugLog.setVisible(false);
		
		rdbtnServer.setVisible(true);
		rdbtnClient.setVisible(true);
	}
	
	public void enableMessageSending(boolean set){
		lblDataSent.setVisible(set);
		textFieldSent.setVisible(set);
		btnSendData.setVisible(set);
	}
	
	public void display(String encrypted, String plain) {
		if(pnDataReceived.getText().split("\n").length > 3 || pnDataReceived.getText().split("\n")[0].length() == 0){
			pnDataReceived.setText(plain);
		}
		else {
			pnDataReceived.setText(pnDataReceived.getText() + '\n' + plain);
		}
		if(pnDataEncryptReceived.getText().split("\n").length > 3 || pnDataEncryptReceived.getText().split("\n")[0].length() == 0){
			pnDataEncryptReceived.setText(encrypted);
		}
		else {
			pnDataEncryptReceived.setText(pnDataEncryptReceived.getText() + '\n' + encrypted);
		}
	}
	
	public void updateLog(String text)
	{
		if(textPaneDebugLog.getText().split("\n").length > 11 || textPaneDebugLog.getText().split("\n")[0].length() == 0){
			textPaneDebugLog.setText(text);
		} else {
			textPaneDebugLog.setText(textPaneDebugLog.getText() + "\n" + text);
		}
	}
	
	public String getIP()
	{
		InetAddress ip;
		try {
			ip = InetAddress.getLocalHost();
			return ip.getHostAddress().toString();
			}
		catch (UnknownHostException e) {	 
			e.printStackTrace();
			}
		return "";
	}
}
