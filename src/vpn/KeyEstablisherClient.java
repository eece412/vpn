package vpn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.sun.org.apache.xml.internal.security.utils.Base64;

public class KeyEstablisherClient {

	private int generator;
	private int prime;
	private VPNGUI gui;
	
	
	public KeyEstablisherClient(int generator, int prime, VPNGUI gui) {
		this.generator = generator;
		this.prime = prime;
		this.gui = gui;
	}
	
	public void authenticate(String sharedSecret, BufferedReader input, PrintWriter output) throws IOException {
		// receive server challenge
		String serverChallenge = input.readLine();
		
		if (Debug.debug) {
			gui.updateLog("Received server challenge: " + serverChallenge);
		}
		
		// send response to server
		int challenge = (int)(Math.random()*10000+1);
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Our developers are idiots. See line 30 of KeyEstablisherClient.java.");
		}
		digest.update((challenge + serverChallenge + sharedSecret).getBytes());
		String response = Base64.encode(digest.digest());
		
		if (Debug.debug) {
			gui.updateLog("client response: " + response + " ready");
			gui.updateLog("client challenge: " + challenge+ " ready");
			gui.updateLog("Click continue to send response and challenge");
			Debug.hold();
		}
		output.println(response);
		output.println(challenge);
		
		// evaluate server response
		String serverResponse = input.readLine();
		
		digest.update((serverChallenge + challenge + sharedSecret).getBytes());
		String hash = Base64.encode(digest.digest());
		if (!serverResponse.equals(hash))
			throw new RuntimeException("Server did not respond correctly to the challenge.");
		
		if (Debug.debug) {
			gui.updateLog("Server responded correctly.");
		}
	}
	
	public int establishKey(BufferedReader input, PrintWriter output) throws NumberFormatException, IOException {
		// get G^B % P
		int serverValue = Integer.valueOf(input.readLine());
		
		if (Debug.debug) {
			gui.updateLog("Received server value (g^b mod p): " + serverValue);
		}
		
		// generate client secret A
		int mySecret = (int)(Math.random()*10+1);
		
		// send G^A % P
		int clientValue = (int)(Math.pow(generator, mySecret)) % prime;
		
		if (Debug.debug) {
			gui.updateLog("Computed client secret (a): " + mySecret);
			gui.updateLog("Computed client value (g^a mod p): " + clientValue);
			gui.updateLog("Click continue to send client value");
			Debug.hold();
		}
		
		output.println(clientValue);
		
		int sessionKey = (int)(Math.pow(serverValue, mySecret)) % prime;
		
		if (Debug.debug) {
			gui.updateLog("Computed Diffie-Hellman session key: " + sessionKey);
		}
		
		// return shared secret: (G^B % P)^A % P
		return sessionKey;
	}
}
