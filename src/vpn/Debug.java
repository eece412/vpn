package vpn;

public class Debug {
	public static volatile boolean debug = false;
	public static volatile boolean step = false;
	
	public static void hold() {
		while (debug && !step);
		step = false;
	}
}
