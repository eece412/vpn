package vpn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;

public class Server implements Runnable {
	private ServerSocket service;
	private Socket socket;
	private KeyEstablisherServer keyEstablisher;
	private Encrypt encryptor;
	private Decrypt decryptor;
	private BufferedReader input = null;
	private PrintWriter output = null;
	private int secretKey;
	private boolean running = false;
	private VPNGUI gui;
	private Thread thread;
	private String secret;
	
	public Server(int port, String secret, int generator, int prime, VPNGUI gui) throws IOException {
		this.gui = gui;
		this.secret = secret;
		service = new ServerSocket(port);
		keyEstablisher = new KeyEstablisherServer(generator, prime, gui);
	}
	
	public void start() throws IOException {
		if (thread == null) {
			thread = new Thread(this, "server");
			thread.start();
		}
	}
	
	public void run() {
		if (socket == null) {
			try{
				socket = service.accept();
				gui.enableMessageSending(true);
				input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				output = new PrintWriter(socket.getOutputStream(), true);
				keyEstablisher.authenticate(secret, input, output);
				secretKey = keyEstablisher.establishKey(input, output);
				encryptor = new Encrypt(ByteBuffer.allocate(16).putInt(secretKey).array(), gui);
				decryptor = new Decrypt(ByteBuffer.allocate(16).putInt(secretKey).array(), gui);
				running = true;
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		while(true){
			try {
				if (!running)
					break;
				if (input.ready()) {
					String encrypted = input.readLine();
					gui.display(encrypted, decryptor.decrypt(encrypted));
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void sendMessage(String message) {
		if (running)
			output.println(encryptor.encrypt(message));
	}
	
	public void stop() throws IOException {
		running = false;
		if (input != null)
			input.close();
		if (output != null)
			output.close();
		if (socket != null)
			socket.close();
		if (service != null)
			service.close();
	}
}
