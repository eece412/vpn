package vpn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.sun.org.apache.xml.internal.security.utils.Base64;

public class KeyEstablisherServer {
	
	private int generator;
	private int prime;
	private VPNGUI gui;
	
	public KeyEstablisherServer(int generator, int prime, VPNGUI gui) {
		this.generator = generator;
		this.prime = prime;
		this.gui = gui;
	}
	
	public void authenticate(String sharedSecret, BufferedReader input, PrintWriter output) throws IOException {
		// send challenge to client
		int challenge = (int)(Math.random()*10000+1);
		
		if (Debug.debug) {
			gui.updateLog("Click continue to send server challenge: " + challenge);
			Debug.hold();
		}
		output.println(challenge);
		
		// receive client response and challenge
		String clientResponse = input.readLine();
		String clientChallenge = input.readLine();
		
		if (Debug.debug) {
			gui.updateLog("Received client response: " + clientResponse);
			gui.updateLog("Received client challenge: " + clientChallenge);
		}
		
		// evaluate client response
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Our developers are idiots. See line 33 of KeyEstablisherServer.java.");
		}
		digest.update((clientChallenge + challenge + sharedSecret).getBytes());
		String hash = Base64.encode(digest.digest());
		if (!clientResponse.equals(hash))
			throw new RuntimeException("Client did not respond correctly to the challenge.");
		
		// send response to client
		digest.update((challenge + clientChallenge + sharedSecret).getBytes());
		String serverResponse = Base64.encode(digest.digest());
		
		if (Debug.debug) {
			gui.updateLog("Client responded correctly.");
			gui.updateLog("Click continue to send server response: " + serverResponse);
			Debug.hold();
		}
		output.println(serverResponse);
	}
	
	public int establishKey(BufferedReader input, PrintWriter output) throws NumberFormatException, IOException {
		// generate server secret B
		int mySecret = (int)(Math.random()*10+1);
		
		// send G^B % P
		int serverValue = (int)(Math.pow(generator, mySecret)) % prime;
		
		if (Debug.debug) {
			gui.updateLog("Computed server secret (b): " + mySecret);
			gui.updateLog("Computed server value (g^b mod p): " + serverValue);
			gui.updateLog("Click continue to send server value...");
			Debug.hold();
		}
		output.println(serverValue);
		
		// get G^A % P
		int clientValue = Integer.valueOf(input.readLine());
		
		int sessionKey = (int)(Math.pow(clientValue, mySecret)) % prime;
		
		if (Debug.debug) {
			gui.updateLog("Received client value (g^a mod p): " + clientValue);
			gui.updateLog("Computed Diffie-Hellman session key: " + sessionKey);
		}
		
		// return shared secret: (G^A % P)^B % P
		return sessionKey;
	}
}
