package vpn;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;

public class Client implements Runnable {
	private Socket socket;
	private KeyEstablisherClient keyEstablisher;
	private Encrypt encryptor;
	private Decrypt decryptor;
	private BufferedReader input = null;
	private PrintWriter output = null;
	private int secretKey;
	private Thread thread;
	private VPNGUI gui;

	private String secret;
	private int generator;
	private int prime;
	private boolean initialized;
	private String message;
	
	
	public Client(String hostName, int port, String secret, int generator, int prime, VPNGUI gui) throws UnknownHostException, IOException {
		socket = new Socket(hostName, port);
		input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		output = new PrintWriter(socket.getOutputStream(), true);
		this.secret = secret;
		this.generator = generator;
		this.prime = prime;
		this.gui = gui;
	}
	
	public void run() {
		if(!initialized){
			initialized = true;
			try{
				keyEstablisher = new KeyEstablisherClient(generator, prime, gui);
				keyEstablisher.authenticate(secret, input, output);
				secretKey = keyEstablisher.establishKey(input, output);
				encryptor = new Encrypt(ByteBuffer.allocate(16).putInt(secretKey).array(), gui);
				decryptor = new Decrypt(ByteBuffer.allocate(16).putInt(secretKey).array(), gui);
			}
			catch(Exception e) {
				throw new RuntimeException("Failed during authentication with server");
			}
		}
			
		while (true) {
			try {
				if (input.ready()) {
					String encrypted = input.readLine();
					gui.display(encrypted, decryptor.decrypt(encrypted));
				}
				else if (message != null){
					sendMessage(message);
					message = null;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void start() {
		if (thread == null) {
			thread = new Thread(this, "client");
			thread.start();
		}
	}
	
	private void sendMessage(String message) throws IOException {
		output.println(encryptor.encrypt(message));
	}
	
	public void setMessage(String message){
		this.message = message;
	}
	
	public void stop() throws IOException {
		if (socket != null) {
			socket.close();
		}
	}
}
