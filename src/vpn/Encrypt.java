package vpn;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import java.util.Random;

import com.sun.org.apache.xml.internal.security.utils.Base64;

public class Encrypt {
	
	private byte[] keyBytes;
	
	private byte[] seed;
	private VPNGUI gui;
	
	public Encrypt(byte[] key, VPNGUI gui) {
		this.keyBytes = key;
		this.gui = gui;
	}
	
	public String encrypt(String message) {
		
		byte[] ciphered = new byte[16];
		
		byte[] iv = this.getIV();
		
	    // Initialization
	    SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
	    IvParameterSpec ivSpec = new IvParameterSpec(iv);
	    
	    byte[] macBytes = null;
	    
	    try {
	    	// Mode
	    	Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
	    	
		    // Encrypt
		    cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
		    ciphered = cipher.doFinal(message.getBytes());
		    SecretKeySpec macKey = new SecretKeySpec(keyBytes, "HmacSHA256");
		    Mac mac = Mac.getInstance(macKey.getAlgorithm());
		    mac.init(macKey);
		    macBytes = mac.doFinal(message.getBytes());
		    
	    } catch(Exception e) {
	    	System.err.println(e);
	    }
	    
	    String encodedMessage = Base64.encode(ciphered);
	    String macMessage = Base64.encode(macBytes);
	    String ivMessage = Base64.encode(iv);
	    String fullMessage = ivMessage + VPNGUI.splitter + encodedMessage + VPNGUI.splitter + macMessage;
	    
	    if (Debug.debug) {
	    	gui.updateLog("Encryption IV (outgoing): " + ivMessage);
		    gui.updateLog("Encrypted message (outgoing): " + encodedMessage);
		    gui.updateLog("MAC (outgoing): " + macMessage);
		    gui.updateLog("Full message (outgoing): " + fullMessage);
		    gui.updateLog("");
	    }
		return fullMessage;
	}
	
	private byte[] getIV() {
		Random randomGenerator = new SecureRandom();
		byte[] iv = new byte[16];
		randomGenerator.nextBytes(iv);
		return iv;
	}
}
