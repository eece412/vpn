package vpn;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;

public class Decrypt {
	
	private byte[] keyBytes;
	
	private byte[] ivBytes;
	private VPNGUI gui;
	
	public Decrypt(byte[] key, VPNGUI gui) {
		com.sun.org.apache.xml.internal.security.Init.init();
		this.keyBytes = key;
		this.gui = gui;
	}
	
	public String decrypt(String message) {
	    
	    String[] parts = message.split(Pattern.quote(VPNGUI.splitter));
	    String iv = parts[0];
	    String cipherText = parts[1];
	    String macString = parts[2];
	    
	    
	    if (Debug.debug) {
		    gui.updateLog("Full message (incoming): " + message);
		    gui.updateLog("Encryption IV (incoming): " + iv);
		    gui.updateLog("Encrypted message (incoming): " + cipherText);
		    gui.updateLog("MAC (incoming): " + macString);
		    gui.updateLog("");
	    }
	    
	    try {
	    	// Mode
			Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
			
			// Initialize
			SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
			IvParameterSpec ivSpec = new IvParameterSpec(Base64.decode(iv));
			
			// Decrypt
			cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);
			byte[] plain = cipher.doFinal(Base64.decode(cipherText));
			try {
				SecretKeySpec macKey = new SecretKeySpec(keyBytes, "HmacSHA256");
				Mac mac = Mac.getInstance(macKey.getAlgorithm());
				mac.init(macKey);
				byte[] macBytes = mac.doFinal(plain);
				if (!Base64.encode(macBytes).equals(macString)) {
					throw new RuntimeException("MAC did not match message. Security may be compromised.");
				}
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
			
			// Print for humans.
			return new String(plain,"UTF-8");
			
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | InvalidAlgorithmParameterException
				| IllegalBlockSizeException | BadPaddingException
				| UnsupportedEncodingException | Base64DecodingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
